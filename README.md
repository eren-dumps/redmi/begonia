## begonia-user 11 RP1A.200720.011 V12.5.5.0.RGGMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6785
- Codename: begonia
- Brand: Redmi
- Flavor: evolution_begonia-userdebug
- Release Version: 13
- Id: TP1A.220905.004.A2
- Incremental: 1664207158
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/begonia/begonia:11/RP1A.200720.011/V12.5.5.0.RGGMIXM:user/release-keys
- OTA version: 
- Branch: begonia-user-11-RP1A.200720.011-V12.5.5.0.RGGMIXM-release-keys
- Repo: redmi/begonia
